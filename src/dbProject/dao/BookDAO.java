package dbProject.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import dbProject.entity.BookDTO;

public class BookDAO {
	private static BookDAO instance = null;
	
	private static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String DB_URI = "몰";
	private static final String DB_USER = "?";
	private static final String DB_PWD = "루";
	
	private BookDAO() {
		// resource에서 DB 커넥션 정보 가져오는 부분 들어가면 됨
	};
	
	public static BookDAO getInstance() {
		if(instance == null) {
			synchronized (BookDAO.class) {
				if(instance == null) {
					instance = new BookDAO();
				}
			}
		}
		return instance;
	}

//	public int insertBook(BookDTO book) {
	public void insertBook(BookDTO book) {
//		int result = -1;
		
		Connection conn = null;
		PreparedStatement st = null;
		try {
			Class.forName(DB_DRIVER);
			conn = DriverManager.getConnection(DB_URI, DB_USER, DB_PWD);
			
			String num = book.getBookNo();
			String title = book.getBookTitle();
			String author = book.getBookAuthor();
			int year = book.getBookYear();
			int price = book.getBookPrice();
			String publisher = book.getBookPublisher();
			
			String sql = "INSERT INTO book "
					+ "(bookNo, bookTitle, bookAuthor, bookYear, bookPrice, bookPublisher) "
					+ "VALUES(?, ?, ?, ?, ?, ?)";
			
			// ?로 나중에 값을 넣을 때는 prepareStatement를 사용하면 된다.
			st = conn.prepareStatement(sql);
			st.setString(1, num);
			st.setString(2, title);
			st.setString(3, author);
			st.setInt(4, year);
			st.setInt(5, price);
			st.setString(6, publisher);
			
			// 변경된 row의 수를 반환한다.
//			result = st.executeUpdate();
			st.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
//		return result;
	}
	
	// 왜 void?
//	public List<BookDTO> selectBook()
	public void selectBook()
	{
//		List<BookDTO> books = new ArrayList<BookDTO>();
		
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			Class.forName(DB_DRIVER);
			conn = DriverManager.getConnection(DB_URI, DB_USER, DB_PWD);
			
			String sql = "SELECT * "
					+ "FROM book";
			
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next()) {
				String bookNo = rs.getString("bookNo");
				String bookTitle = rs.getString("bookTitle");
				String bookAuthor = rs.getString("bookAuthor");
				int bookYear = rs.getInt("bookYear");
				int bookPrice = rs.getInt("bookPrice");
				String bookPublisher = rs.getString("bookPublisher");

//				books.add(new BookDTO(bookNo, bookTitle, bookAuthor,
//						bookYear, bookPrice, bookPublisher));
				System.out.printf("%s\t%-12s\t%s\t%d\t%d\t\s\n",
						bookNo, bookTitle, bookAuthor, bookYear, bookPrice, bookPublisher);
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) {
					rs.close();
					rs = null;
				}
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
//		return books;
	}
}
